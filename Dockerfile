FROM java:8
ADD ./fatjar/jwt-restapi-example-0.0.1-SNAPSHOT.jar jwt-restapi-example-0.0.1-SNAPSHOT.jar
RUN bash -c 'touch jwt-restapi-example-0.0.1-SNAPSHOT.jar'
RUN mkdir -p /logdata
VOLUME /logdata
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar jwt-restapi-example-0.0.1-SNAPSHOT.jar
EXPOSE 8080
