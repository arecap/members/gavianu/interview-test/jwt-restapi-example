package org.arecap.gavianu.jwtrestapiexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class JwtRestapiExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtRestapiExampleApplication.class, args);
	}

}
