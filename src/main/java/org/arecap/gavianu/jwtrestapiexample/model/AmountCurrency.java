package org.arecap.gavianu.jwtrestapiexample.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AmountCurrency {

    private Double amount;

    private String currency;

}
