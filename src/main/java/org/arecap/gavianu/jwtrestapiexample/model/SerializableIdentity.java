package org.arecap.gavianu.jwtrestapiexample.model;

import java.io.Serializable;

public interface SerializableIdentity extends Serializable {

    String getId();

    void setId(String id);

}
