package org.arecap.gavianu.jwtrestapiexample.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class Account implements SerializableIdentity, TimestampUpdateAware {

    private String id;

    private Date update;

    private String name;

    private String product;

    private String status;

    private String type;

    private Double balance;

}
