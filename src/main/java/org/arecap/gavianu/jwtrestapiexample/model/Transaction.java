package org.arecap.gavianu.jwtrestapiexample.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class Transaction implements SerializableIdentity, TimestampUpdateAware {

    private String id;

    private String accountId;

    private ExchangeRate exchangeRate;

    private AmountCurrency originalAmount;

    private ThirdParty creditor;

    private ThirdParty debtor;

    private String status;

    private String currency;

    private Double amount;

    private Date update;

    private String description;


}
