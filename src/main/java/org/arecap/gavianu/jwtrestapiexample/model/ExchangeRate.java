package org.arecap.gavianu.jwtrestapiexample.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExchangeRate {

    private String currencyFrom;

    private String currencyTo;

    private Double rate;


    public static ExchangeRate eurUsd() {
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setCurrencyFrom("EUR");
        exchangeRate.setCurrencyTo("USD");
        exchangeRate.setRate(1.1);
        return exchangeRate;
    }

    public static ExchangeRate usdEur() {
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setCurrencyFrom("USD");
        exchangeRate.setCurrencyTo("EUR");
        exchangeRate.setRate(1/1.1);
        return exchangeRate;
    }


    public static ExchangeRate eurRon() {
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setCurrencyFrom("EUR");
        exchangeRate.setCurrencyTo("RON");
        exchangeRate.setRate(4.84);
        return exchangeRate;
    }

    public static ExchangeRate ronEur() {
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setCurrencyFrom("RON");
        exchangeRate.setCurrencyTo("EUR");
        exchangeRate.setRate(1/4.84);
        return exchangeRate;
    }

    public static ExchangeRate usdRon() {
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setCurrencyFrom("USD");
        exchangeRate.setCurrencyTo("RON");
        exchangeRate.setRate(4.09);
        return exchangeRate;
    }


    public static ExchangeRate ronUsd() {
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setCurrencyFrom("RON");
        exchangeRate.setCurrencyTo("USD");
        exchangeRate.setRate(1/4.09);
        return exchangeRate;
    }

}
