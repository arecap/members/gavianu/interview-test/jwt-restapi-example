package org.arecap.gavianu.jwtrestapiexample.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ThirdParty {

    private String maskedPan;

    private String name;

}
