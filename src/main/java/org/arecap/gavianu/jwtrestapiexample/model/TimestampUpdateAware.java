package org.arecap.gavianu.jwtrestapiexample.model;

import java.util.Date;

public interface TimestampUpdateAware {

    void setUpdate(Date date);

}
