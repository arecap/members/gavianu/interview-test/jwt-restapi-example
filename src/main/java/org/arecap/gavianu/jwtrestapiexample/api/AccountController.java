package org.arecap.gavianu.jwtrestapiexample.api;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.arecap.gavianu.jwtrestapiexample.model.Account;
import org.arecap.gavianu.jwtrestapiexample.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/accounts")
    @Parameters({@Parameter(in = ParameterIn.HEADER, name = "X-AUTH", schema = @Schema(type = "string"))})
    public List<Account> getAllTransactions() {
        return accountService.findAll();
    }

}
