package org.arecap.gavianu.jwtrestapiexample.service;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.arecap.gavianu.jwtrestapiexample.model.Account;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Random;
import java.util.stream.IntStream;


@Service
@Slf4j
public class AccountService extends LocalStorageModelService<Account> {

    private static String[] accountProducts = {"Temporary account.", "Silver account.", "Gold account.", "Premimum.account"};

    private static String[] accountStatuses = {"ENABLE", "DISABLE"};

    private static String[] accountTypes = {"DEBIT_CARD", "CREDIT_CARD"};

//    @Scheduled(cron = "0 20 16 * * ?")
    @Scheduled(fixedRate = 86_400_000L)
    public void loadAccounts() {
//        log.info("load account every day at 16:20PM executed at:\t" + LocalDate.now());
        log.info("load accounts once at 24h (fixRate= 86.400.000 millis):\t" + LocalDate.now());
        final Faker faker = new Faker();
        final Random random = new Random();
        IntStream.range(0, 100).parallel().forEach(i -> loadAccount(faker, random));
    }


    private void loadAccount(Faker faker, Random random) {
        Account account = new Account();
        account.setName(faker.name());
        account.setProduct(accountProducts[random.nextInt(4)]);
        account.setStatus(accountStatuses[random.nextInt(2)]);
        account.setType(accountTypes[random.nextInt(2)]);
        double leftLimit = -10_000D;
        double rightLimit = 100_000D;
        account.setBalance(leftLimit + random.nextDouble() * (rightLimit - leftLimit));
        save(account);
    }


}
