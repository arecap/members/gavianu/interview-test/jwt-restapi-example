package org.arecap.gavianu.jwtrestapiexample.service;

import lombok.extern.slf4j.Slf4j;
import org.arecap.gavianu.jwtrestapiexample.model.AmountCurrency;
import org.arecap.gavianu.jwtrestapiexample.model.ExchangeRate;
import org.arecap.gavianu.jwtrestapiexample.model.ThirdParty;
import org.arecap.gavianu.jwtrestapiexample.model.Transaction;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;

@Service
@Slf4j
public class TransactionService extends LocalStorageModelService<Transaction> {


    private static ExchangeRate[] exchangeRates = {ExchangeRate.eurUsd(), ExchangeRate.usdEur(),
                ExchangeRate.eurRon(), ExchangeRate.ronEur(), ExchangeRate.usdRon(), ExchangeRate.ronUsd()};


    private static String[] transactionStatuses = {"REJECTED", "BOOKED", "PENDING", "PAID"};

    @Scheduled(fixedRate = 86_400_000L)
    public void loadTransactions() {
        log.info("load transactions once at 24h (fixRate= 86.400.000 millis):\t" + LocalDate.now());
        IntStream.range(0, 100).parallel().forEach(this::loadTransaction);
    }

    private void loadTransaction(int index) {
        Random random = new Random();
        Transaction transaction = new Transaction();
        transaction.setAccountId(UUID.randomUUID().toString());
        transaction.setExchangeRate(exchangeRates[random.nextInt(exchangeRates.length)]);
        transaction.setOriginalAmount(mockAmountCurrency(random,
                transaction.getExchangeRate().getCurrencyTo()));
        transaction.setCreditor(mockThirdParty(random, "Creditor"));
        transaction.setDebtor(mockThirdParty(random, "DebtorName"));
        transaction.setStatus(transactionStatuses[random.nextInt(4)]);
        transaction.setCurrency(transaction.getExchangeRate().getCurrencyFrom());
        transaction.setAmount(transaction.getOriginalAmount().getAmount() / transaction.getExchangeRate().getRate());
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 25;
        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        transaction.setDescription(generatedString + " transaction - " + index);
        save(transaction);
    }

    private ThirdParty mockThirdParty(Random random, String type) {
        ThirdParty thirdParty = new ThirdParty();
        String id = String.format("%04d", random.nextInt(10000));
        thirdParty.setMaskedPan("XXXXXXXXXX" + id);
        thirdParty.setName(type + " " + id);
        return thirdParty;
    }

    private AmountCurrency mockAmountCurrency(Random random, String currencyTo) {
        AmountCurrency amountCurrency = new AmountCurrency();
        amountCurrency.setCurrency(currencyTo);
        double leftLimit = 0.1D;
        double rightLimit = 2_000D;
        amountCurrency.setAmount(leftLimit + random.nextDouble() * (rightLimit - leftLimit));
        return amountCurrency;
    }


}
