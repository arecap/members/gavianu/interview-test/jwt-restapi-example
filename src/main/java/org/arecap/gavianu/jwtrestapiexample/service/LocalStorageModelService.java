package org.arecap.gavianu.jwtrestapiexample.service;

import org.arecap.gavianu.jwtrestapiexample.model.SerializableIdentity;
import org.arecap.gavianu.jwtrestapiexample.model.TimestampUpdateAware;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public abstract class LocalStorageModelService<T extends SerializableIdentity> {

    //Dummy local storage repo
    private Map<String, T> modelStorage = new ConcurrentHashMap<>();

    public T save(T model) {
        if(!Optional.ofNullable(model.getId()).isPresent()) {
            model.setId(UUID.randomUUID().toString());
        }
        if(TimestampUpdateAware.class.isAssignableFrom(model.getClass())) {
            ((TimestampUpdateAware)model).setUpdate(new Date());
        }
        modelStorage.put(model.getId(), model);
        return model;
    }

    public List<T> findAll() {
        return modelStorage.values().stream().collect(Collectors.toList());
    }

}
