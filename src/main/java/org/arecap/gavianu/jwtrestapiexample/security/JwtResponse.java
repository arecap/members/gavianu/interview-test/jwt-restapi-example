package org.arecap.gavianu.jwtrestapiexample.security;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter @Setter
public class JwtResponse implements Serializable {

    private String token;

    //need default constructor for JSON Parsing - use in test
    public JwtResponse() {
    }

    public JwtResponse(String token) {
        this.token = token;
    }

}
