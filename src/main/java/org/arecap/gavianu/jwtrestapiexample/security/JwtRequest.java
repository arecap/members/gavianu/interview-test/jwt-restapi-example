package org.arecap.gavianu.jwtrestapiexample.security;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter @Setter
public class JwtRequest implements Serializable {


    private String username;

    //need default constructor for JSON Parsing
    public JwtRequest() {
    }

    public JwtRequest(String username) {
        setUsername(username);
    }

}
