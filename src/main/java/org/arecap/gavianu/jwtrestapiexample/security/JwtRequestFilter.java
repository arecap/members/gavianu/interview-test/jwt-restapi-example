package org.arecap.gavianu.jwtrestapiexample.security;


import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private DummyUserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        final String jwtToken = request.getHeader("X-AUTH");
        String username = null;
        if(SecurityContextHolder.getContext().getAuthentication() == null && jwtToken != null) {
            log.info("request have X-AUTH header token:\t" + jwtToken + "\nRequest jwt authentication");
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                log.error("Unable to get JWT Token", e.getStackTrace());
            } catch (ExpiredJwtException e) {
                log.error("JWT Token has expired", e.getStackTrace());
            }

        }
        // Once we get the token validate it.
        if (username != null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            // if token is valid configure Spring Security to manually set
            // authentication
            if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
                log.info("login success");
                DummyUserDetailsService.authenticateRequest(request, userDetails);
            }
        }
        chain.doFilter(request, response);
    }


}
