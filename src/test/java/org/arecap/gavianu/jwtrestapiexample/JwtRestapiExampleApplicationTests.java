package org.arecap.gavianu.jwtrestapiexample;

import org.arecap.gavianu.jwtrestapiexample.security.JwtRequest;
import org.arecap.gavianu.jwtrestapiexample.security.JwtResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class JwtRestapiExampleApplicationTests {


	@Autowired
	private TestRestTemplate restTemplate;
	@Test
	void contextLoads() {
		ResponseEntity<JwtResponse> jwtResponse =  restTemplate.postForEntity("/login", new JwtRequest("gavianu"), JwtResponse.class);
		Assert.isTrue(jwtResponse.getStatusCode().equals(HttpStatus.OK), "login fail!");

	}

}
