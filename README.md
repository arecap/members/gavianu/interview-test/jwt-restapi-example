# jwt-restapi-example

This project use Spring Boot Framework to build and executable jar that feature application features in a JVM compliant application


## Application requirements

Customer Accounts Aggregation

 

You need to build an aggregation REST API to expose accounts and transactions for the users you have in your application. You need to connect to another provider to get this data. This provider can return accounts and transactions for any user. You need to call the /login endpoint and use the received JWT in the other requests. You can find the documentation here and how to run it: https://hub.docker.com/repository/docker/mihaitatinta/wiremock-example

docker run -it --rm -p 8080:8080 mihaitatinta/wiremock-example
Requirements:

You need to create 2 endpoints to expose accounts and transactions from your local application storage (anything you want - the faster, the better)
The accounts and transactions need to be updated 1 time per day in your application.
Exception handling
Nice to have: tests
Frontend is optional
Readme: how to build and run your application
Pushed to github/gitlab or similar.
Spring boot/Java 8+ are recommended but not mandatory
If you can't run Docker on your machine, you can run your own wiremock server from here: https://hub.docker.com/repository/docker/mihaitatinta/wiremock-example

## Project comprehension

### Build

It is a maven project. 
In a maven compliant running environment: use     $mvn -install
$./mvnw command you'll build the project in any running linux & mac os base running env

### Run 
After build maven will create the runable fat jar in ./target/jwt-restapi-example-0.0.1-SNAPSHOT.jar

In JVM compliant running env $java -jar jwt-restapi-example-0.0.1-SNAPSHOT.jar in target folder

With docker:

docker run -p 8080:8080 registry.gitlab.com/arecap/members/gavianu/interview-test/jwt-restapi-example/jwt-restapi-example:latest

#### Running and test
It can be tested with Open Api the Swagger UI and the Open Api are ouside the security context

on local env use:
http://localhost:8080/swagger-ui.html  


### Framework and dependency


Application use Spring Boot to publish a IoC enable Java Applicationand and Scheduling 

Application use Spring Boot Starter Web for enable MVC REST-API enable by @RestController components

Application use Spring Boot Starter Security for enable secure the WebContext and Service call
 
Application use Springdoc Open Api and expose Swagger UI for test and documentation the application API endpoints 
 
Application use lombok for spicing up java code
  
  
  